import * as types from './actionConstants';
import axios from 'axios';
import { apiURL } from '../utils/apiUtils';

export const initAction = (data) => ({ type: types.INIT, data });
export const clearAction = { type: types.CLEAR };

export const postForm = async (data,token) => {
    console.log(data)
    const url = apiURL+'/api/auth/form';
    const result = await axios.post(url,{
        headers: {
            'Content-Type':  'application/json',
            'x-access-token': token
        },
        data,
    }).then(function (response) {
        return response.data.text
    }).catch(function (err) {
        return err
    })
    return {
        type: types.SUBMIT_DATA,
        payload: result
    }
};
