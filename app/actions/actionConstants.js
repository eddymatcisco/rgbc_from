// Global UI Action
export const TOGGLE_SIDEBAR = 'TOGGLE_SIDEBAR';
export const LOAD_PAGE = 'LOAD_PAGE';

// Redux Form
export const INIT = 'INIT';
export const CLEAR = 'CLEAR';

// Crud Form Table
export const FETCH_DATA_FORM = 'FETCH_DATA_FORM';
export const ADD_NEW = 'ADD_NEW';
export const CLOSE_FORM = 'CLOSE_FORM';
export const SUBMIT_DATA = 'SUBMIT_DATA';
export const REMOVE_ROW_FORM = 'REMOVE_ROW_FORM';
export const EDIT_ROW_FORM = 'EDIT_ROW_FORM';
