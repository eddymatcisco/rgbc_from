export const apiURL = createAPIURL();

function createAPIURL() {
    let hostname = window.location.hostname;

    switch (hostname) {

        case 'localhost':
            return "http://localhost:8081";

        default:
            return "http://localhost:8081";
    }
}