import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import { Field, reduxForm } from 'redux-form/immutable';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Grid from '@material-ui/core/Grid';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { TimePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import {DropzoneArea} from 'material-ui-dropzone'
import MomentUtils from '@date-io/moment';
import Typography from '@material-ui/core/Typography';
import { TextField } from '@material-ui/core';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import SignatureCanvas from 'react-signature-canvas';
import ReactS3 from 'react-s3';
import {
  CheckboxRedux,
  SelectRedux,
  SwitchRedux
} from './ReduxFormMUI';
import { postForm } from '../../../actions/ReduxFormActions';
import brandData from '../../../api/data';


const renderRadioGroup = ({ input, ...rest }) => (
  <RadioGroup
    {...input}
    {...rest}
    valueselected={input.value}
    onChange={(event, value) => input.onChange(value)}
  />
);

// validation functions
const required = value => (value == null ? 'Required' : undefined);

const useStyles = makeStyles(theme => ({
  root: {
    width: 300 + 24 * 2,
    padding: 24,
  },
  margin: {
    height: theme.spacing(3),
  },
  demo: {
    height: 'auto',
  },
  divider: {
    margin: `${theme.spacing(3)}px 0`,
  }
})); 


const styles = theme => ({
  root: {
    flexGrow: 1,
    padding: 30
  },
  field: {
    width: '100%',
    marginBottom: 20
  },
  fieldBasic: {
    width: '100%',
    marginBottom: 20,
    marginTop: 10
  },
  inlineWrap: {
    display: 'flex',
    flexDirection: 'row'
  },
  buttonInit: {
    margin: theme.spacing(4),
    textAlign: 'center'
  },
  error: {
    fontSize:9,
    color:'red'
  },
  divider: {
    margin: `${theme.spacing(3)}px 0`,
  },
  picker: {
    margin: `${theme.spacing(3)}px 5px`,
  }
});

const config = {
  bucketName: 'rgbcfilebucket',
  region: 'eu-west-2',
  accessKeyId: 'AKIAYLH6RYCIXCOOESU7',
  secretAccessKey: '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',
}

class ReduxForm extends Component {
  state = {
    userId: this.props.match.params.userId,
	  userName: this.props.match.params.userName,
	  regionName: this.props.match.params.regionName,
	  storeName: this.props.match.params.storeName,
    storeCode: this.props.match.params.storeCode,
    token: this.props.match.params.token,
    brand:'',
    startTime: new Date(),
    promoterName:'',
    isStoreManagerAware:'',
    isThereSufficientStock:'',
    isTherePromotionalPoint:'',
    isThereDisplayBrand:'',
    isBriefedOnBrand:'',
    isBriefedOnPromotion:'',
    promotionalElements: [],
    didYouReceiveMixers:'',
    whatMixers:'',
    howManyPromotersBooked:'',
    inStorePromotion:'',
    wereAnyKitsUsed:'',
    howMany: 0,
    howManyPeopleWereExposed:  0,
    consumersResponseToBrand:'',
    promotionalElementsTwo: [],
    brandRetailSellingPrice: '',
    averageGender:'',
    averageAge:'',
    generalComments:'',
    stockSoldAtThePromotion:'',
    storeManageName:'',
    venueRating:'',
    endTime: new Date(),
    trimmedDataURL: null,
    signature:'',
    filesOne: [],
    filesTwo: [],
    filesThree: [],
    filesFour: [],
  }

  sigPad = {}
  clear = () => {
    this.sigPad.clear()
  }

  handleStartTimeChange = time => {
    this.setState({ startTime: time });
  }

  handleEndTimeChange = time => {
    this.setState({ endTime: time });
  }

  handleFieldChange = (e) => {
    console.log(e.target)
    console.log(e.target.name+ " " + e.target.value)
    if(e.target.name === 'brand'){
      this.setState({brand:e.target.value});
    }
    if(e.target.name === 'start_time'){
      this.setState({startTime:e.target.value});
    }
    if(e.target.name === 'promoter_name'){
      this.setState({promoterName:e.target.value});
    }
    if(e.target.name === 'is_store_manager_aware'){
      this.setState({isStoreManagerAware:e.target.value});
    }
    if(e.target.name === 'is_there_sufficient_stock'){
      this.setState({isThereSufficientStock:e.target.value});
    }
    if(e.target.name === 'is_there_promotional_point'){
      this.setState({isTherePromotionalPoint:e.target.value});
    }
    if(e.target.name === 'is_there_a_display_brand'){
      this.setState({isThereDisplayBrand:e.target.value});
    }
    if(e.target.name === 'briefed_on_the_brand'){
      this.setState({isBriefedOnBrand:e.target.value});
    }
    if(e.target.name === 'briefed_on_the_promotion'){
      this.setState({isBriefedOnPromotion:e.target.value});
    }
    if(e.target.name === 'branded_sampling_table' || e.target.name === 'generic_sampling_table' || e.target.name === 'ice_bucket' || e.target.name === 'pull_up_banners' || e.target.name === 'sampling_cups' || e.target.name === 'tongs' || e.target.name === 'give_aways' || e.target.name === 'all_of_the_above' ){
      if(!e.target.value){
        this.setState({promotionalElements:[...this.state.promotionalElements, e.target.name]});
      }else{
        const array = [...this.state.promotionalElements];
        const index = array.indexOf(e.target.value)
        if (index !== -1) {
          array.splice(index, 1);
          this.setState({promotionalElements: array});
        }
      }
    }
    if(e.target.name === 'did_you_receive_mixers'){
      this.setState({didYouReceiveMixers:e.target.value});
    }
    if(e.target.name === 'what_mixers'){
      this.setState({whatMixers:e.target.value});
    }
    if(e.target.name === 'how_many_promoters_booked'){
      this.setState({howManyPromotersBooked:e.target.value});
    }
    if(e.target.name === 'in_store_promotion'){
      this.setState({inStorePromotion:e.target.value});
    }
    if(e.target.name === 'were_any_kits_used_for_this_promotion'){
      this.setState({wereAnyKitsUsed:e.target.value});
    }
    if(e.target.name === 'how_many'){
      this.setState({howMany:e.target.value});
    }
    if(e.target.name === 'how_many_people_were_exposed_to_the_promotion'){
      this.setState({howManyPeopleWereExposed:e.target.value});
    }
    if(e.target.name === 'consumers_response_to_the_brand'){
      this.setState({consumersResponseToBrand:e.target.value});
    }
    if(e.target.name === 'management_well_briefed_on_the_promotion' || e.target.name === 'the_venue_was_busy' || e.target.name === 'the_brand_was_a_promotional_special_price' || e.target.name === 'the_instant_redemption_or_value_add' || e.target.name === 'value_added_products' || e.target.name === 'product_tasting_trial'){
      if(!e.target.value){
        this.setState({promotionalElementsTwo:[...this.state.promotionalElementsTwo, e.target.name]});
      }else{
        const array = [...this.state.promotionalElementsTwo];
        const index = array.indexOf(e.target.value)
        if (index !== -1) {
          array.splice(index, 1);
          this.setState({promotionalElementsTwo: array});
        }
      }
    }
    if(e.target.name === 'brand_retail_selling_price'){
      this.setState({brandRetailSellingPrice:e.target.value});
    }
    if(e.target.name === 'average_gender'){
      this.setState({averageGender:e.target.value});
    }
    if(e.target.name === 'average_age'){
      this.setState({averageAge:e.target.value});
    }
    if(e.target.name === 'general_comments'){
      this.setState({generalComments:e.target.value});
    }
    if(e.target.name === 'how_much_stock_was_sold_at_the_promotion'){
      this.setState({stockSoldAtThePromotion:e.target.value});
    }
    if(e.target.name === 'store_manager_name'){
      this.setState({storeManageName:e.target.value});
    }
    if(e.target.name === 'venue_rating'){
      this.setState({venueRating:e.target.value});
    }
  }

  validate = (objects) => {
  if (objects.brand===''){
      document.getElementById("brand_error").innerHTML = "This question is requier!"
      return false 
  } 

  if (objects.promoterName===''){
      document.getElementById("promoter_name_error").innerHTML = "This question is requier!"
      return false 
  } 

  if (objects.sStoreManagerAware===''){
      document.getElementById("is_store_manager_aware_error").innerHTML = "This question is requier!"
      return false 
  } 

  if (objects.isThereSufficientStock===''){
      document.getElementById("is_there_sufficient_stock_error").innerHTML = "This question is requier!"
      return false 
  } 

  if (objects.isTherePromotionalPoint===''){
      document.getElementById("is_there_promotional_point_error").innerHTML = "This question is requier!"
      return false 
  } 

  if (objects.isThereDisplayBrand===''){
      document.getElementById("is_there_a_display_brand_error").innerHTML = "This question is requier!"
      return false 
  } 

  if (objects.isBriefedOnBrand===''){
      document.getElementById("briefed_on_the_brand_error").innerHTML = "This question is requier!"
      return false 
  } 

  if (objects.promotionalElements.length === 0){
      document.getElementById("promotional_elements_error").innerHTML = "This question is requier!"
      return false 
  } 
 
  if (objects.howManyPromotersBooked.length === ''){
    document.getElementById("booked_promotion_error").innerHTML = "This question is requier!"
    return false 
  } 
 
  if (objects.inStorePromotion===''){
      document.getElementById("in_store_promotion_error").innerHTML = "This question is requier!"
      return false 
  } 

  if (objects.consumersResponseToBrand===''){
      document.getElementById("consumers_response_to_the_brand_error").innerHTML = "This question is requier!"
      return false 
  } 

  if (objects.promotionalElementsTwo.length === 0){
      document.getElementById("promotional_elements_1error").innerHTML = "This question is requier!"
      return false 
  } 

  if (objects.brandRetailSellingPrice===''){
      document.getElementById("brand_retail_selling_price_error").innerHTML = "This question is requier!"
      return false 
  } 

   if (objects.averageGender===''){
      document.getElementById("average_gender_error").innerHTML = "This question is requier!"
      return false 
  } 

  if (objects.averageAge===''){
      document.getElementById("average_age_error").innerHTML = "This question is requier!"
      return false 
  } 

  if (objects.generalComments===''){
      document.getElementById("general_comments_error").innerHTML = "This question is requier!"
      return false 
  }

  if (objects.stockSoldAtThePromotion===''){
      document.getElementById("stock_was_sold_at_the_promotion_error").innerHTML = "This question is requier!"
      return false 
  }
  
  if (objects.storeManageName===''){
      document.getElementById("store_manager_name_error").innerHTML = "This question is requier!"
      return false 
  }

  if (objects.venueRating===''){
      document.getElementById("venue_rating_error").innerHTML = "This question is requier!"
      return false 
  }

  // if (objects.trimmedDataURL===null){
  //     document.getElementById("signature_error").innerHTML = "This question is requier!"
  //     return false 
  // }

  if (objects.filesOne.length===0){
      document.getElementById("promoter_sampling_table_error").innerHTML = "This question is requier!"
      return false 
  }

  if (objects.filesTwo.length===0){
      document.getElementById("photo_of_display_error").innerHTML = "This question is requier!"
      return false 
  }

  if (objects.filesThree.length===0){
      document.getElementById("brand_display_on_shell_error").innerHTML = "This question is requier!"
      return false 
  }

  if (objects.filesFour.length===0){
      document.getElementById("consumer_interaction_error").innerHTML = "This question is requier!"
      return false 
  }
  return true
  }

  handleSliderChange = (event, newValue) => {
    setValue(newValue);
  };

  onInputChange=(value)=>{
    console.log(value)
      this.setState({promoterName:value});
  }

  handleFileOneChange = (files) => {
    console.log(files[0])
    ReactS3.uploadFile(files[0],config)
    .then((data)=>{
      console.log(data.location);
      this.setState({filesOne:[...this.state.filesOne, data.location]});
    })
  }

  handleFileTwoChange = (files) => {
    console.log(files[0])
    ReactS3.uploadFile(files[0],config)
    .then((data)=>{
      console.log(data.location);
      this.setState({filesTwo:[...this.state.filesTwo, data.location]});
    })
  }

  handleFileThreeChange = (files) => {
    console.log(files[0])
    ReactS3.uploadFile(files[0],config)
    .then((data)=>{
      console.log(data.location);
      this.setState({filesThree:[...this.state.filesThree, data.location]});
    })
  }

  handleFileFourChange = (files) => {
    console.log(files[0])
    ReactS3.uploadFile(files[0],config)
    .then((data)=>{
      console.log(data.location);
      this.setState({filesFour:[...this.state.filesFour, data.location]});
    })
  }
 
  clear = () => {
    this.sigPad.clear()
  }

  trim = () => {

    if(this.sigPad.isEmpty()){
      document.getElementById("signature_error").innerHTML = "This question is requier!"
    }else{
      this.setState({trimmedDataURL: this.sigPad.getTrimmedCanvas()
        .toDataURL('image/png')})
  
        ReactS3.uploadFile(this.dataURLtoFile(this.sigPad.getTrimmedCanvas()
        .toDataURL('image/png'), `signature${Math.random().toString(36).slice(2)}.png`),config)
        .then((data)=>{
          console.log(data.location);
          this.setState({signature:data.location});
        })
    }
  }

  dataURLtoFile(dataurl, filename) {
 
    var arr = dataurl.split(','),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), 
        n = bstr.length, 
        u8arr = new Uint8Array(n);
        
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    
    return new File([u8arr], filename, {type:mime});
}

  handleSubmit = (e) => {
    e.preventDefault()
    this.trim()
    console.log(this.state)
    if(this.validate(this.state)){
      postForm(this.state,this.state.token)
    }
  };

  render() {
    const { startTime, endTime, promoterName, whatMixers, howMany, didYouReceiveMixers, wereAnyKitsUsed, venueRating, files } = this.state;
    const trueBool = true;
    const {classes} = this.props;
    return (
      <div>
        <Grid container spacing={3} alignItems="flex-start" direction="row" justify="center">
          <Grid item xs={12} md={6}>
            <Paper className={classes.root}>
              <Typography variant="h5" component="h3">
                RGBC Form 
              </Typography>
              <form onSubmit={this.handleSubmit}>
                <div className={classes.fieldBasic}>
                  <FormControl className={classes.field}>
                    <InputLabel htmlFor="brand">Select a Brand*</InputLabel>
                    <Field
                      name="brand"
                      component={SelectRedux}
                      placeholder="Select a Brand*"
                      onChange={this.handleFieldChange}
                      autoWidth={trueBool}
                      validate={required}
                      required > 
                    {brandData.map((item, key) => {
                      return <MenuItem key={key} value={item}>{item}</MenuItem>
                    })}   
                    </Field>
                    <span id='brand_error' className={classes.error} ></span>
                  </FormControl>
                </div>
                <div className={classes.picker}>
                  <MuiPickersUtilsProvider utils={MomentUtils}>
                    <TimePicker
                      clearable
                      ampm={false}
                      name='start_time'
                      label="StartTime*"
                      value={startTime}
                      onChange={this.handleStartTimeChange}
                    />
                  </MuiPickersUtilsProvider>
                  <span id='start_time_error' className={classes.error} ></span>
                </div>
                <div className={classes.fieldBasic}>
                  <TextField
                    name="promoter_name"
                    placeholder="Promoter Name and Surname*"
                    label="Promoter Name and Surname*"
                    value={promoterName}
                    onChange={this.handleFieldChange}
                    className={classes.field}
                  />
                  <span id='promoter_name_error' className={classes.error} ></span>
                </div>
                <div className={classes.fieldBasic}>
                  <FormLabel component="label">Is The Store Manager Aware of This Activity?*</FormLabel>
                  <Field name="is_store_manager_aware_field" className={classes.inlineWrap} component={renderRadioGroup} >
                    <FormControlLabel value="Yes" control={<Radio name="is_store_manager_aware" onChange={this.handleFieldChange} />} label="Yes" />
                    <FormControlLabel value="No" control={<Radio name="is_store_manager_aware" onChange={this.handleFieldChange} />} label="No" />
                  </Field>
                  <span id='is_store_manager_aware_error' className={classes.error} ></span>
                </div>
                <div className={classes.fieldBasic}>
                  <FormLabel component="label">Is There Sufficient Stock On Shelf?*</FormLabel>
                  <Field name="is_there_sufficient_stock_field" className={classes.inlineWrap} component={renderRadioGroup} required>
                    <FormControlLabel value="Yes" control={<Radio name="is_there_sufficient_stock" onChange={this.handleFieldChange} />} label="Yes" />
                    <FormControlLabel value="No" control={<Radio name="is_there_sufficient_stock" onChange={this.handleFieldChange} />} label="No" />
                  </Field>
                  <span id='is_there_sufficient_stock_error' className={classes.error} ></span>
                </div>
                <div className={classes.fieldBasic}>
                  <FormLabel component="label">Is There Promotional Point Of Sale?*</FormLabel>
                  <Field name="is_there_promotional_point_field" className={classes.inlineWrap} component={renderRadioGroup} required>
                    <FormControlLabel value="Yes" control={<Radio name="is_there_promotional_point" onChange={this.handleFieldChange} />} label="Yes" />
                    <FormControlLabel value="No" control={<Radio name="is_there_promotional_point" onChange={this.handleFieldChange} />} label="No" />
                  </Field>
                  <span id='is_there_promotional_point_error' className={classes.error} ></span>
                </div>
                <div className={classes.fieldBasic}>
                  <FormLabel component="label">Is There A Display For The Brand?*</FormLabel>
                  <Field name="is_there_a_display_brand_field" className={classes.inlineWrap} component={renderRadioGroup} required>
                    <FormControlLabel value="Yes" control={<Radio name="is_there_a_display_brand" onChange={this.handleFieldChange} />} label="Yes" />
                    <FormControlLabel value="No" control={<Radio name="is_there_a_display_brand" onChange={this.handleFieldChange} />} label="No" />
                  </Field>
                  <span id='is_there_a_display_brand_error' className={classes.error} ></span>
                </div>
                <div className={classes.fieldBasic}>
                  <FormLabel component="label">Have You Been Briefed On The Brand?*</FormLabel>
                  <Field name="briefed_on_the_brand_field" className={classes.inlineWrap} component={renderRadioGroup} required>
                    <FormControlLabel value="Yes" control={<Radio name="briefed_on_the_brand" onChange={this.handleFieldChange} />} label="Yes" />
                    <FormControlLabel value="No" control={<Radio name="briefed_on_the_brand" onChange={this.handleFieldChange} />} label="No" />
                  </Field>
                  <span id='briefed_on_the_brand_error' className={classes.error} ></span>
                </div>
                <div className={classes.fieldBasic}>
                  <FormLabel component="label">Have You Been Briefed On The Promotion?*</FormLabel>
                  <Field name="briefed_on_the_promotion_field" className={classes.inlineWrap} component={renderRadioGroup} required>
                    <FormControlLabel value="Yes" control={<Radio name="briefed_on_the_promotion" onChange={this.handleFieldChange} />} label="Yes" />
                    <FormControlLabel value="No" control={<Radio name="briefed_on_the_promotion" onChange={this.handleFieldChange} />} label="No" />
                  </Field>
                  <span id='briefed_on_the_promotion_error' className={classes.error} ></span>
                </div>
                <div className={classes.fieldBasic}>
                  <FormLabel component="label">Have You Been Given The Following Promotional Elements?*</FormLabel>
                  <div className={classes.rows}>
                    <FormControlLabel control={<Field name="branded_sampling_table" onChange={this.handleFieldChange} component={CheckboxRedux} />} label="Branded Sampling Table" />
                    <FormControlLabel control={<Field name="generic_sampling_table" onChange={this.handleFieldChange} component={CheckboxRedux} />} label="Generic Sampling Table" />
                    <FormControlLabel control={<Field name="ice_bucket" onChange={this.handleFieldChange} component={CheckboxRedux} />} label="Ice Bucket" />
                    <FormControlLabel control={<Field name="pull_up_banners" onChange={this.handleFieldChange} component={CheckboxRedux} />} label="Pull up Banners" />
                    <FormControlLabel control={<Field name="sampling_cups" onChange={this.handleFieldChange} component={CheckboxRedux} />} label="Sampling Cups" />
                    <FormControlLabel control={<Field name="tongs" onChange={this.handleFieldChange} component={CheckboxRedux} />} label="Tongs" />
                    <FormControlLabel control={<Field name="give_aways" onChange={this.handleFieldChange} component={CheckboxRedux} />} label="Give aways" />
                    <FormControlLabel control={<Field name="all_of_the_above" onChange={this.handleFieldChange} component={CheckboxRedux} />} label="All of the above" />
                  </div>
                  <span id='promotional_elements_error' className={classes.error} ></span>
                </div>
                <div className={classes.fieldBasic}>
                  <FormLabel component="label">Did You Receive Mixers?</FormLabel>
                  <Field name="did_you_receive_mixers_field" className={classes.inlineWrap} component={renderRadioGroup} >
                    <FormControlLabel value="Yes" control={<Radio name="did_you_receive_mixers" onChange={this.handleFieldChange} />} label="Yes" />
                    <FormControlLabel value="No" control={<Radio name="did_you_receive_mixers" onChange={this.handleFieldChange} />} label="No" />
                  </Field>
                </div>
                {didYouReceiveMixers === 'Yes' ?
                  <div className={classes.fieldBasic}>
                    <TextField
                      name="what_mixers"
                      placeholder="What Mixers?"
                      label="What Mixers?"
                      value={whatMixers}
                      ref={this.saveRef}
                      onChange={this.handleFieldChange}
                      className={classes.field}
                    />
                  </div>
                  :
                  <div></div>
                }
                <div className={classes.fieldBasic}>
                  <FormLabel component="label">How Many Promoters Have Been Booked For This Promotion?*</FormLabel>
                  <Field name="how_many_promoters_booked_field" className={classes.inlineWrap} component={renderRadioGroup}>
                    <FormControlLabel value="1 Promoter" control={<Radio name="how_many_promoters_booked" onChange={this.handleFieldChange} />} label="1 Promoter" />
                    <FormControlLabel value="2 Promoter" control={<Radio name="how_many_promoters_booked" onChange={this.handleFieldChange} />} label="2 Promoter" />
                    <FormControlLabel value="3 Promoter" control={<Radio name="how_many_promoters_booked" onChange={this.handleFieldChange} />} label="3 Promoter" />
                    <FormControlLabel value="4 Promoter" control={<Radio name="how_many_promoters_booked" onChange={this.handleFieldChange} />} label="4 Promoter" />
                  </Field>
                  <span id='booked_promotion_error' className={classes.error} ></span>
                </div>
                <div className={classes.fieldBasic}>
                  <FormLabel component="label">Type Of Promotion? In store promotion</FormLabel>
                  <Field name="in_store_promotion_field" className={classes.inlineWrap} component={renderRadioGroup}>
                    <FormControlLabel value="Yes" control={<Radio name="in_store_promotion" onChange={this.handleFieldChange}/>} label="Yes" />
                    <FormControlLabel value="No" control={<Radio name="in_store_promotion" onChange={this.handleFieldChange}/>} label="No" />
                  </Field>
                  <span id='in_store_promotion_error' className={classes.error} ></span>
                </div>
                <div className={classes.fieldBasic}>
                  <FormLabel component="label">Were Any Kits Used For This Promotion?</FormLabel>
                  <Field name="were_any_kits_used_for_this_promotion_field" className={classes.inlineWrap} component={renderRadioGroup} >
                    <FormControlLabel value="Yes" control={<Radio name="were_any_kits_used_for_this_promotion" onChange={this.handleFieldChange} />} label="Yes" />
                    <FormControlLabel value="No" control={<Radio name="were_any_kits_used_for_this_promotion" onChange={this.handleFieldChange} />} label="No" />
                  </Field>
                </div>
                {wereAnyKitsUsed === 'Yes' ?
                  <div className={classes.fieldBasic}>
                    <TextField
                      name="how_many"
                      type="number"
                      placeholder="How Many?"
                      label="How Many?"
                      onChange={this.handleFieldChange}
                      value={howMany}
                      ref={this.saveRef}
                      className={classes.field}
                    />
                  </div>
                  :
                  <div></div>
                }
                <div className={classes.fieldBasic}>
                  <TextField
                    name="how_many_people_were_exposed_to_the_promotion"
                    type="number"
                    placeholder="How Many People Were Exposed To The Promotion?*"
                    label="How Many People Were Exposed To The Promotion?*"
                    onChange={this.handleFieldChange}                  
                    className={classes.field}
                  />
                  <span id='how_many_people_were_exposed_error' className={classes.error} ></span>
                </div>
                <div className={classes.fieldBasic}>
                  <FormLabel component="label">What Was The Consumers Response To The Brand?*</FormLabel>
                  <Field name="consumers_response_to_the_brand_field" className={classes.inlineWrap} component={renderRadioGroup}>
                    <FormControlLabel value="Positive" control={<Radio name="consumers_response_to_the_brand" onChange={this.handleFieldChange} />} label="Positive" />
                    <FormControlLabel value="Negative" control={<Radio name="consumers_response_to_the_brand" onChange={this.handleFieldChange} />} label="Negative" />
                  </Field>
                  <span id='consumers_response_to_the_brand_error' className={classes.error} ></span>
                </div>
                <div className={classes.fieldBasic}>
                  <FormLabel component="label">Have You Taken A Photo Of The Promoter And Sampling Table?*</FormLabel>
                  <DropzoneArea 
                    showPreviews={true}
                    onChange={this.handleFileOneChange}
                  />
                  <span id='promoter_sampling_table_error' className={classes.error} ></span>
                </div>
                <div className={classes.fieldBasic}>
                  <FormLabel component="label">Have You Taken A Photo Of The Display?*</FormLabel>
                  <DropzoneArea 
                    showPreviews={true}
                    onChange={this.handleFileTwoChange}
                  />
                  <span id='photo_of_display_error' className={classes.error} ></span>
                </div>
                <div className={classes.fieldBasic}>
                  <FormLabel component="label">Have You Taken A Photo Of The Brand Displayed On Shelf?*</FormLabel>
                  <DropzoneArea 
                    showPreviews={true}
                    onChange={this.handleFileThreeChange}
                  />
                  <span id='brand_display_on_shell_error' className={classes.error} ></span>
                </div>
                <div className={classes.fieldBasic}>
                  <FormLabel component="label">Have You Taken A Photo Of The Consumer Interaction With The Brand?*</FormLabel>
                  <DropzoneArea 
                    showPreviews={true}
                    onChange={this.handleFileFourChange}
                  />
                  <span id='consumer_interaction_error' className={classes.error} ></span>
                </div>
                <div className={classes.fieldBasic}>
                  <FormLabel component="label">Have You Been Given The Following Promotional Elements?*</FormLabel>
                  <div className={classes.rows}>
                    <FormControlLabel control={<Field name="management_well_briefed_on_the_promotion" onChange={this.handleFieldChange} component={CheckboxRedux} />} label="Management well briefed on the promotion" />
                    <FormControlLabel control={<Field name="the_venue_was_busy" onChange={this.handleFieldChange} component={CheckboxRedux} />} label="The Venue was busy" />
                    <FormControlLabel control={<Field name="the_brand_was_a_promotional_special_price" onChange={this.handleFieldChange} component={CheckboxRedux} />} label="The Brand was a promotional/special price" />
                    <FormControlLabel control={<Field name="the_instant_redemption_or_value_add" onChange={this.handleFieldChange} component={CheckboxRedux} />} label="The instant redemption or value add" />
                    <FormControlLabel control={<Field name="value_added_products" onChange={this.handleFieldChange} component={CheckboxRedux} />} label="Value Added Products" />
                    <FormControlLabel control={<Field name="product_tasting_trial" onChange={this.handleFieldChange} component={CheckboxRedux} />} label="Product Tasting/Trial" />
                  </div>
                  <span id='promotional_element_1_error' className={classes.error} ></span>
                </div>
                <div className={classes.fieldBasic}>
                  <FormLabel component="label">Please Rate How Busy The Venue Was From 1-5? (1 Being Very Quiet 5 Being Very Busy)?*</FormLabel>
                  <Field name="venue_rating_field" className={classes.inlineWrap} component={renderRadioGroup}>
                    <FormControlLabel value="1"  control={<Radio name="venue_rating" onChange={this.handleFieldChange} />} label=" 1 " />
                    <FormControlLabel value="2"  control={<Radio name="venue_rating" onChange={this.handleFieldChange} />} label=" 2 " />
                    <FormControlLabel value="3"  control={<Radio name="venue_rating" onChange={this.handleFieldChange} />} label=" 3 " />
                    <FormControlLabel value="4"  control={<Radio name="venue_rating" onChange={this.handleFieldChange} />} label=" 4 " />
                    <FormControlLabel value="5"  control={<Radio name="venue_rating" onChange={this.handleFieldChange} />} label=" 5 " />
                  </Field>
                  <span id='venue_rating_error' className={classes.error} ></span>
                </div>
                <div className={classes.field}>
                  <TextField
                    name="brand_retail_selling_price"
                    className={classes.field}
                    onChange={this.handleFieldChange}
                    placeholder="Brand Retail Selling Price*"
                    label="Brand Retail Selling Price*"
                  />
                  <span id='brand_retail_selling_price_error' className={classes.error} ></span>
                </div>
                <div className={classes.fieldBasic}>
                  <FormLabel component="label">Average Gender Of The Consumers Interested In The Brand?*</FormLabel>
                  <Field name="average_gender_field" className={classes.inlineWrap} component={renderRadioGroup}>
                    <FormControlLabel value="Male" control={<Radio name="average_gender" onChange={this.handleFieldChange} />} label="Male" />
                    <FormControlLabel value="Female" control={<Radio name="average_gender" onChange={this.handleFieldChange} />} label="Female" />
                  </Field>
                  <span id='average_gender_error' className={classes.error} ></span>
                </div>
                <div className={classes.fieldBasic}>
                  <FormLabel component="label">Average Age Of The Consumers Interested In The Brand?*</FormLabel>
                  <Field name="average_age_field" className={classes.inlineWrap} component={renderRadioGroup}>
                    <FormControlLabel value="18 - 24" control={<Radio name="average_age" onChange={this.handleFieldChange} />} label="18 - 24" />
                    <FormControlLabel value="25 - 29" control={<Radio name="average_age" onChange={this.handleFieldChange} />} label="25 - 29" />
                    <FormControlLabel value="30 - 39" control={<Radio name="average_age" onChange={this.handleFieldChange} />} label="30 - 39" />
                    <FormControlLabel value="40 - 49" control={<Radio name="average_age" onChange={this.handleFieldChange} />} label="40 - 49" />
                    <FormControlLabel value="50+" control={<Radio name="average_age" onChange={this.handleFieldChange} />} label="50+" />
                  </Field>
                  <span id='average_age_error' className={classes.error} ></span>
                </div>
                <div className={classes.field}>
                  <TextField
                    name="general_comments"
                    className={classes.field}
                    placeholder="General Comments*"
                    onChange={this.handleFieldChange}
                    label="General Comments*"
                  />
                  <span id='general_comments_error' className={classes.error} ></span>
                </div>
                <div className={classes.field}>
                  <TextField
                    name="how_much_stock_was_sold_at_the_promotion"
                    type="number"
                    className={classes.field}
                    placeholder="How Much Stock (In Bottles) Was Sold At The Promotion? (If Rtd's State Case Quantity)*"
                    onChange={this.handleFieldChange}
                    label="How Much Stock (In Bottles) Was Sold At The Promotion? (If Rtd's State Case Quantity)*"
                  />
                  <span id='stock_was_sold_at_the_promotion_error' className={classes.error} ></span>
                </div>
                <div className={classes.field}>
                  <TextField
                    name="store_manager_name"
                    className={classes.field}
                    placeholder="Store Manager Name?*"
                    onChange={this.handleFieldChange}
                    label="Store Manager Name?*"
                  />
                  <span id='store_manager_name_error' className={classes.error} ></span>
                </div>
                <div className={classes.picker}>
                  <MuiPickersUtilsProvider utils={MomentUtils}>
                    <TimePicker
                      clearable
                      ampm={false}
                      name="end_time"
                      label="End Time*"
                      value={endTime}
                      onChange={this.handleEndTimeChange}
                    />
                  </MuiPickersUtilsProvider>
                  <span id='end_time_error' className={classes.error} ></span>
                </div>
                <div className={classes.field}>
                  <FormLabel component="label">Signature*</FormLabel>
                  <SignatureCanvas penColor='black' canvasProps={{width: 500, height: 200, className: 'sigCanvas'}} ref={(ref) => { this.sigPad = ref }} />
                  <span id='signature_error' className={classes.error} ></span>
                </div>
                <div>
                  <Button variant="contained" color="secondary" type="submit">
                    Submit
                  </Button>
                </div>
              </form>
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

renderRadioGroup.propTypes = {
  input: PropTypes.object.isRequired,
};

ReduxForm.propTypes = {
  classes: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
  pristine: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
};

const mapDispatchToProps = dispatch => ({
 
});

const ReduxFormMapped = reduxForm({
  form: 'immutableExample',
  enableReinitialize: true,
})(ReduxForm);

const reducer = 'initval';
const FormInit = connect(
  state => ({
    force: state,
    initialValues: state.getIn([reducer, 'formValues'])
  }),
  mapDispatchToProps,
)(ReduxFormMapped);

export default withStyles(styles)(FormInit);
