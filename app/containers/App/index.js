import React from 'react';
import { Switch, Route } from 'react-router-dom';
import {
  Form,
  NotFound
} from '../pageListAsync';
window.__MUI_USE_NEXT_TYPOGRAPHY_VARIANTS__ = true;


class App extends React.Component {
  render() {
    return (
            <Switch>
              <Route path="/:userId/:userName/:regionName/:storeName/:storeCode/:token" exact component={Form} />
              <Route component={NotFound} />
            </Switch>
    );
  }
}

export default App;
