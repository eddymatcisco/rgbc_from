import React from 'react';
import { Helmet } from 'react-helmet';

import { Route } from 'react-router-dom';
import { ErrorWrap } from 'dan-components';

const title = 'RGBC - Page Not Found';

const NotFound = () => (
  <Route
    render={({ staticContext }) => {
      if (staticContext) {
        staticContext.status = 404; // eslint-disable-line
      }
      return (
        <div>
          <Helmet>
            <title>{title}</title>
            <meta property="og:title" content={title} />
            <meta property="twitter:title" content={title} />
          </Helmet>
          <ErrorWrap title="404" desc="Oops, Page Not Found :(" />
        </div>
      );
    }}
  />
);

export default NotFound;
