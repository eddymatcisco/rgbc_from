const brandData= [
'Ardbeg',
'Belvedere',
'Benriach',
'Chambord',
'Disaronno',
'Dom Perignon',
'Drambuie',
'El Jimador',
'Finlandia',
'Gentleman Jack',
'Glendronach',
'Glenglassaugh',
'Glenmorangie',
'Hendricks',
'Hennessy',
'Herradura',
'Jack Daniels',
'Jack Daniels And Apple',
'Jack Daniels And Cola',
'Jack Daniels Fire',
'Jack Daniels Frank Sinatra',
'Jack Daniels Honey',
'Jack Daniels Single Barrel',
'Krug',
'Moet and Chandon',
'Monkey Shoulder',
'Reyka Vodka',
'RGBC',
'RGBC Competitors',
'Ruinart',
'Sailor Jerry',
'Southern Comfort',
'Southern Comfort Lime',
'Southern Comfort Lime and Soda',
'Tia Maria',
'Tullamore Dew',
'Tullamore Dew 12YO',
'Veuve Clicquot',
'Wixworth',
'Woodford Double Oaked',
'Woodford Reserve',
'Southern Comfort Black'
]

export default brandData;